// Module to organize all the imports. Remove unused in particular.
use std::path::PathBuf;

use walker::FileList;
use model::{AdaFile, AdaFileBuilder};

/// Pair of the two files that pertain to an Ada package
#[derive(Debug, PartialEq)]
struct PackagePair {
    spec: AdaFile,
    body: AdaFile,
}

#[derive(PartialEq)]
enum UnusedImports<'a> {
    /// Unused import located in the spec
    UnusedSpec(&'a str),
    /// Unused import located in the body
    UnusedBody(&'a str),
    /// Import made in both the spec and the body
    Dupe(&'a str),
    /// Import in spec but only used in body (may need to move import to body unless also have
    /// a Dupe for the same import)
    OnlyBody(&'a str),
}

/// Entry function for the the organizing of the imports. That is the removal of all unused imports
/// and ensuring that if the package is imported in the spec that it isn't imported again in the
/// body.
pub fn organize_imports(files: &FileList) -> FileList {
    let pairs = group_files(files);
    let mut return_list = vec![];
    for pair in &pairs {
        let unused = find_unused_imports(pair);
        let result_pair = rewrite_imports(pair, unused);
        return_list.push(result_pair.spec);
        return_list.push(result_pair.body);
    }
    return_list
}

// TODO Add better error handling by justifing or removing all unwrap
/// Group all specs with the associated body files.
fn group_files(files: &FileList) -> Vec<PackagePair> {
    debug!("Grouping files");
    // Main principle here will be to first compare the file names
    // Is safe to do unwrap as extension check has been done in walker module
    let specs: Vec<PathBuf> = files.into_iter()
        .map(|x| x.get_path())
        .filter(|x| "ads".eq(x.extension().unwrap()))
        .collect();
    let bodies: Vec<PathBuf> = files.into_iter()
        .map(|x| x.get_path())
        .filter(|x| "adb".eq(x.extension().unwrap()))
        .collect();

    let mut pairs = vec![];
    for spec in specs {
        // TODO Add error handling
        let file_name = spec.file_stem().unwrap();
        // TODO Add error handling
        let matched =
            bodies.iter().filter(|x| x.file_stem().unwrap().eq(file_name)).next().unwrap();
        pairs.push(PackagePair {
            spec: AdaFileBuilder::new().path(&spec).build(),
            body: AdaFileBuilder::new().path(matched).build(),
        });
        debug!("Created pair of {:?}", pairs[pairs.len() - 1]);
    }
    pairs
}

/// Given a body and spec, find all the unused imports and label them appropriately
fn find_unused_imports(pair: &PackagePair) -> Vec<UnusedImports> {
    let mut unused_imports = vec![];
    // Get the list of all imports
    let spec_imports = pair.spec.extract_imports();
    let body_imports = pair.body.extract_imports();

    debug!("Spec imports: {:?}", spec_imports);
    debug!("Body imports: {:?}", body_imports);
    // Apply some rules
    // - If in spec shouldn't be in body
    for import in &spec_imports {
        if body_imports.contains(import) {
            info!("Body contains import also in spec: {:?}", import);
            unused_imports.push(UnusedImports::Dupe(import));
        }
    }
    // - If not used in spec/body should be removed
    // Spec test
    debug!("Organizing the spec");
    for import in &spec_imports {
        let occurences = count_occurences(import, &pair.spec);
        debug!("{:?} occurs {:?} time(s)", import, occurences);
        if occurences < 1 {
            // TODO Maybe look into keeping the line number or something
            let body_occ = count_occurences(import, &pair.body);
            if body_occ >= 1 {
                info!("Import in spec but only used in body: {:?}", import);
                unused_imports.push(UnusedImports::OnlyBody(import));
            } else {
                info!("Removing unused import {:?}", import);
                unused_imports.push(UnusedImports::UnusedSpec(import));
            }
        }
    }
    // Body test
    debug!("Organizing the body");
    for import in &body_imports {
        let occurences = count_occurences(import, &pair.body);
        debug!("{:?} occurs {:?} time(s)", import, occurences);
        if occurences < 1 {
            info!("Removing unused import {:?}", import);
            unused_imports.push(UnusedImports::UnusedBody(import));
        }
    }

    unused_imports
}

/// Count the number of occurences of the provided package to check how many times it is
/// used in the provided file
fn count_occurences(package: &str, file: &AdaFile) -> usize {
    // NOTE This assumes spaces are used not tabs, like they should be ;)
    let mut search_string = String::from(" ");
    search_string.push_str(package);
    search_string.push_str(".");

    file.get_content().matches(&search_string).count()
}


fn rewrite_imports(pair: &PackagePair, unused_imports: Vec<UnusedImports>) -> PackagePair {
    let mut spec_file =
        AdaFileBuilder::new().path(&pair.spec.get_path()).content(&pair.spec.get_content()).build();
    let mut body_file =
        AdaFileBuilder::new().path(&pair.body.get_path()).content(&pair.body.get_content()).build();

    for unused_import in unused_imports {
        match unused_import {
            UnusedImports::Dupe(import) => body_file.remove_package_import(import),
            UnusedImports::OnlyBody(import) => {
                spec_file.remove_package_import(import);
                body_file.insert_package_import(import);
            }
            UnusedImports::UnusedBody(import) => body_file.remove_package_import(import),
            UnusedImports::UnusedSpec(import) => spec_file.remove_package_import(import),
        }
    }

    PackagePair {
        spec: spec_file,
        body: body_file,
    }

}


#[cfg(test)]
mod tests {
    use std::path::{Path, PathBuf};
    use std::fs::{self, File};
    use std::io::Write;
    use model::AdaFile;

    #[test]
    fn group_files_test() {
        let tester = vec![AdaFile::from(PathBuf::from("tests/resources/test.adb")),
                          AdaFile::from(PathBuf::from("tests/resources/test.ads")),
                          AdaFile::from(PathBuf::from("tests/resources/test.a")),
                          AdaFile::from(PathBuf::from("tests/resources/test_b.a"))];
        let result = super::group_files(&tester);

        assert_eq!(result[0],
                   super::PackagePair {
                       spec: AdaFile::from(PathBuf::from("tests/resources/test.ads")),
                       body: AdaFile::from(PathBuf::from("tests/resources/test.adb")),
                   });
        // TODO Need to add code to handle .a extension
    }

    // FIXME Think of better way to use the same code across different test modules
    // Utility functions
    fn create_test_file<P: AsRef<Path>>(path: P, content: &str) -> AdaFile {
        let path: &Path = path.as_ref();
        let mut file = File::create(path).unwrap();
        let content = String::from(content);
        file.write_all(&(content.into_bytes())[..]).unwrap();
        AdaFile::from(path.to_path_buf())
    }

    fn remove_test_file<P: AsRef<Path>>(path: P) {
        fs::remove_file(path).unwrap();
    }

    #[test]
    fn count_occurences_test() {
        let path = "tests/resources/count_test.ada_test";
        let content = "with not_needed;
with needed;
with test_package;

package body test is
  needed.do();
  not_needed.do()
  needed_not.do()
end package;";
        let ada_file = create_test_file(path, content);
        let result = super::count_occurences("needed", &ada_file);
        assert_eq!(result, 1);
        remove_test_file(path);
    }

    #[test]
    fn find_unused_imports_test() {
        let pair = super::PackagePair {
            spec: AdaFile::from(PathBuf::from("tests/resources/test.ads")),
            body: AdaFile::from(PathBuf::from("tests/resources/test.adb")),
        };
        let unused = super::find_unused_imports(&pair);
        assert!(unused.contains(&super::UnusedImports::UnusedSpec("not_used")));
        assert!(unused.contains(&super::UnusedImports::OnlyBody("only_used_in_body")));
        assert!(unused.contains(&super::UnusedImports::UnusedBody("not_needed")));
        assert!(unused.contains(&super::UnusedImports::Dupe("test_package")));
        // TODO Think of more dynamic way to check these are the only ones
        assert_eq!(unused.len(), 4);
    }
}
