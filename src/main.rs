extern crate adafmt;
extern crate clap;
extern crate env_logger;
#[macro_use]
extern crate log;

use clap::{Arg, App};
use std::error::Error;


fn main() {
    let _ = env_logger::init();

    let matches = App::new("Adafmt")
        .version("1.0.0")
        .author("Dylan M.")
        .about("Formatter for Ada files")
        .arg(Arg::with_name("imports")
            .short("m")
            .long("organize")
            .help("Organizies imports of the provided project")
            .takes_value(false))
        .arg(Arg::with_name("PROJECT")
            .help("Root directory of the project")
            .required(true)
            .index(1))
        .get_matches();

    // unwrap is safe as the PROJECT value is required
    let project_dir = matches.value_of("PROJECT").unwrap();
    println!("Formatting project at {:?}", project_dir);

    match adafmt::find_ada_files(project_dir) {
        Ok(files_to_format) => {
            let final_files = if matches.is_present("imports") {
                info!("Organizing imports");
                adafmt::organize_imports(&files_to_format)
            } else {
                vec![]
            };

            // Once all has been done write the files in memory back to file system
            adafmt::write_back_ada_files(final_files);
        }
        Err(error) => {
            error!("Unable to walk the project directory, cause: {:?}",
                   error.cause().map(|err| err.description()).unwrap_or(error.description()));
            println!("For more information run again with 'RUST_LOG=adafmt ./adafmt <args>'");
        }
    }
}
