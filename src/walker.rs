// Module to handle the walking of the src directory
// and obtaining the path to all the correct files.
// Perhaps want to look into this converting it into the types thinking about
// where the spec and body are grouped together.
extern crate walkdir;

use self::walkdir::{WalkDir, Error};
use std::path::Path;

use model::AdaFile;


pub type FileList = Vec<AdaFile>;

pub fn find_ada_files(root_dir: &str) -> Result<FileList, Error> {
    let mut files = vec![];
    for entry in WalkDir::new(root_dir) {
        let entry = try!(entry);

        let path = entry.path();
        if is_ada_file(path) {
            files.push(AdaFile::from(path.to_owned()));
        }

    }
    Ok(files)
}

fn is_ada_file(path: &Path) -> bool {
    const FILE_EXTENSIONS: [&'static str; 3] = ["a", "adb", "ads"];
    let mut result = false;
    for ext in &FILE_EXTENSIONS {
        if let Some(extension) = path.extension().and_then(|x| x.to_str()) {
            if (*ext).eq(extension) {
                debug!("{:?} accepted as Ada file", path);
                result = true;
                break;
            }
        }
    }
    result
}

#[cfg(test)]
mod tests {
    use std::path::{Path, PathBuf};

    #[test]
    fn find_ada_files_test() {
        let files = super::find_ada_files("tests/resources").unwrap();
        let files: Vec<PathBuf> = files.iter().map(|x| x.get_path()).collect();
        assert!(files.contains(&PathBuf::from("tests/resources/test_b.a")));
        assert!(files.contains(&PathBuf::from("tests/resources/test.a")));
        assert!(files.contains(&PathBuf::from("tests/resources/test.adb")));
        assert!(files.contains(&PathBuf::from("tests/resources/test.ads")));
    }

    #[test]
    fn is_ada_file_test() {
        assert!(!super::is_ada_file(Path::new("bad_test.c")));
        assert!(super::is_ada_file(Path::new("good.a")));
        assert!(super::is_ada_file(Path::new("good_test_b.a")));
        assert!(super::is_ada_file(Path::new("good_test.ads")));
        assert!(super::is_ada_file(Path::new("good_test.adb")));
    }


}
