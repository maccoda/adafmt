/// Module to perform the writing of all the formatted files back to the project.
use std::error::Error;
use std::fs::{self, File};
use std::io::{self, Write};
use std::process;

use walker::FileList;
use model::AdaFile;

pub fn write_files(files: FileList) {
    debug!("Beginning write back of formatted files");
    for file in files {
        write_file(file).unwrap_or_else(|err: io::Error| {
            writeln!(&mut io::stderr(),
                     "ERROR: Unable to write output, {:?}",
                     err.description())
                .expect("Could not write to stdout");
            process::exit(1)
        });
    }
}

fn write_file(file: AdaFile) -> io::Result<()> {
    debug!("Writing back {:?}", file.get_path());
    // Check that the file already existed
    let attr = try!(fs::metadata(file.get_path()));
    if attr.is_file() {
        // TODO Would be nice to display the changes that will be making, perhaps may need
        // to be part of the type that keeps track of changes to a file
        // Overwrite the file
        Ok(try!(File::create(file.get_path())
            .and_then(|mut x| x.write_all(&(file.get_content().into_bytes())[..]))))

    } else {
        warn!("Path is a directory");
        Err(io::Error::new(io::ErrorKind::InvalidInput, "Path is a directory"))
    }
}
