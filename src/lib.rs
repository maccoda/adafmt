#[macro_use]
extern crate log;
extern crate walkdir;

mod walker;
mod config;
mod formatting;
mod writer;
pub mod model;

pub use self::model::AdaFile;


use walkdir::Error;

pub fn write_back_ada_files(files: walker::FileList) {
    writer::write_files(files)
}

pub fn find_ada_files(root_dir: &str) -> Result<walker::FileList, Error> {
    walker::find_ada_files(root_dir)
}

pub fn organize_imports(files: &walker::FileList) -> walker::FileList {
    formatting::imports::organize_imports(files)
}
