use std::path::PathBuf;
use std::fs::File;
use std::io::Read;

pub struct AdaFileBuilder {
    path: Option<PathBuf>,
    content: Option<String>,
}

impl AdaFileBuilder {
    pub fn new() -> AdaFileBuilder {
        AdaFileBuilder {
            path: None,
            content: None,
        }
    }
    /// Builder pattern functions
    pub fn path(&mut self, path: &PathBuf) -> &mut AdaFileBuilder {
        self.path = Some(path.to_path_buf());
        self
    }
    pub fn content(&mut self, content: &String) -> &mut AdaFileBuilder {
        self.content = Some(content.to_owned());
        self
    }

    pub fn build(&self) -> AdaFile {
        match self.path {
            Some(ref path) => {
                // Allow client to assume both fields will be populated
                match self.content {
                    Some(ref c) => {
                        AdaFile {
                            path: path.to_path_buf(),
                            content: c.to_owned(),
                        }
                    }
                    None => AdaFile::from(path.to_path_buf()),
                }
            }
            None => {
                warn!("No path set for Adafile!");
                panic!();
            }
        }
    }
}

/// Ada file meta data, path to file and the contents of the file. The contents will be lazily
/// populated, and if there is an error when reading the contents it will also be empty.
#[derive(Debug, PartialEq)]
pub struct AdaFile {
    path: PathBuf,
    content: String,
}

impl AdaFile {
    /// Returns a new AdaFile and populates the content by reading the provided path
    /// panic! if unable to open or read the file.
    // TODO Try move this to similar structure as File::create where can take string or path
    pub fn from(path: PathBuf) -> AdaFile {
        // Using unwrap as want to fail if unable to open file.
        let mut file = File::open(&path).unwrap();
        let mut contents = String::new();
        file.read_to_string(&mut contents).unwrap();
        let content = contents.clone();
        AdaFile {
            path: path,
            content: content,
        }
    }

    /// Remove the import of the provided package from the content of the file.
    // NOTE With the move to this module not sure if this should be here
    pub fn remove_package_import(&mut self, package: &str) {
        let import_line = build_import_line(package);
        info!("Removing {:?} import from {:?}", import_line, self.path);

        let end_content =
            self.content.clone().split(&import_line).take(2).fold(String::new(), |mut l, r| {
                l.push_str(r.trim_left());
                l
            });
        self.content = end_content;
    }

    /// Insert the provided import at the end of the import list
    pub fn insert_package_import(&mut self, package: &str) {
        // FIXME This is not the best method but until have AST can't think of much better
        debug!("Inserting package import: {:?}", package);
        // TODO Need a much better search string, look into regex
        let split = "package ";
        let clone_content = self.content.clone();
        let mut content_iter = clone_content.split(&split);
        let mut final_content = String::from(content_iter.next().unwrap());
        final_content.trim();
        debug!("first line {:?}", final_content);
        // Add new import
        let import_line = build_import_line(package);
        final_content.push_str(&import_line);
        final_content.push_str("\n\n");
        // Add rest
        final_content.push_str(split);
        final_content.push_str(content_iter.next().unwrap());
        self.content = final_content;
    }

    /// Find all the imports used in the file
    // NOTE With the move to this module not sure this should be here
    pub fn extract_imports(&self) -> Vec<&str> {
        const IMPORT_FILTER: &'static str = "with ";
        self.content
            .lines()
            .filter(|line| line.starts_with(IMPORT_FILTER))
            .map(|line| line.split_at(IMPORT_FILTER.len()).1)
            .map(|line| line.split(";").next().unwrap())
            .collect()
    }

    pub fn get_path(&self) -> PathBuf {
        self.path.to_owned()
    }

    pub fn get_content(&self) -> String {
        self.content.to_owned()
    }
}

fn build_import_line(import: &str) -> String {
    const IMPORT_FILTER: &'static str = "with ";
    let mut import_line = String::from(IMPORT_FILTER);
    import_line.push_str(import);
    import_line.push(';');
    import_line
}
#[cfg(test)]
mod tests {
    use std::path::{Path, PathBuf};
    use std::fs::{self, File};
    use std::io::Write;

    // Utility functions
    fn create_test_file<P: AsRef<Path>>(path: P, content: &str) -> super::AdaFile {
        let path: &Path = path.as_ref();
        let mut file = File::create(path).unwrap();
        let content = String::from(content);
        file.write_all(&(content.into_bytes())[..]).unwrap();
        super::AdaFile::from(path.to_path_buf())
    }

    fn remove_test_file<P: AsRef<Path>>(path: P) {
        fs::remove_file(path).unwrap();
    }


    // AdaFile tests
    #[test]
    fn adafile_from() {
        // NOTE Tests are run concurrently so cannot give actual ada extension
        // as it causes the find ada file test to fail
        let path = "tests/resources/test_read.ada_test";
        let test_content = "Hello World!";
        let ada_file = create_test_file(path, test_content);
        assert_eq!(ada_file.get_path(), PathBuf::from(path));
        assert_eq!(ada_file.get_content(), test_content);
        remove_test_file(path);
    }

    #[test]
    fn adafile_extract_imports() {
        let path = "tests/resources/test_extract.ada_test";
        let test_content = "with not_needed;
with needed;
with test_package;

package body test \
                            is
needed.do();
test_package.do();
only_used_in_body.do();
end \
                            package;";

        let ada_file = create_test_file(path, test_content);
        let imports = ada_file.extract_imports();
        assert!(imports.contains(&"not_needed"));
        assert!(imports.contains(&"needed"));
        assert!(imports.contains(&"test_package"));

        remove_test_file(path);
    }

    #[test]
    fn adafile_remove_package_import() {
        let path = "tests/resources/test_remove_import.ada_test";
        let test_content = "with not_needed;
with needed;
with test_package;

package body test \
                            is
needed.do();
test_package.do();
only_used_in_body.do();
end \
                            package;";

        let mut ada_file = create_test_file(path, test_content);
        ada_file.remove_package_import("not_needed");
        assert!(!ada_file.extract_imports().contains(&"not_needed"));
        assert!(ada_file.extract_imports().contains(&"needed"));
        assert!(ada_file.extract_imports().contains(&"test_package"));
        remove_test_file(path);
    }

    // AdaFileBuilder tests
    #[test]
    #[should_panic]
    fn adafilebuilder_build_fail() {
        let builder = super::AdaFileBuilder::new();

        builder.build();
    }

    #[test]
    fn adafilebuilder_build() {
        let mut builder = super::AdaFileBuilder::new();
        let path = PathBuf::from("test_path");
        let test_content = "Testing all the content";
        builder.path(&path);
        builder.content(&String::from(test_content));
        let result = builder.build();
        assert_eq!(result.get_content(), test_content);
        assert_eq!(result.get_path(), path);

        let mut builder = super::AdaFileBuilder::new();
        let path = "tests/resources/builder_test.ada_test";
        let content = "Hello all out there";
        create_test_file(path, content);
        builder.path(&PathBuf::from(path));
        let result = builder.build();
        assert_eq!(result.get_content(), content);
        assert_eq!(result.get_path(), PathBuf::from(path));
        remove_test_file(path);
    }
}
