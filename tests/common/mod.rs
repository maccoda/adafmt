use std::fs::File;
use std::io::{Read, Write};
use std::path::Path;

pub fn read_file<P: AsRef<Path>>(path: P) -> String {
    let mut file = File::open(path.as_ref()).unwrap();
    let mut contents = String::new();
    file.read_to_string(&mut contents).unwrap();
    contents
}

pub fn write_file<P: AsRef<Path>>(path: P, content: String) {
    let path: &Path = path.as_ref();
    let mut file = File::create(path).unwrap();
    file.write_all(&(content.into_bytes())[..]).unwrap();
}
