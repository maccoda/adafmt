with not_needed;
with needed;
with test_package;

package body test is
  needed.do();
  test_package.do();
  only_used_in_body.do();
end package;
