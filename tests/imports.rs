extern crate adafmt;

use std::path::PathBuf;

use adafmt::AdaFile;
use common::{read_file, write_file};


mod common;
#[test]
fn organize_imports() {
    // Have a good file kept and the one to format.
    let spec_path = "tests/resources/test.ads";
    let body_path = "tests/resources/test.adb";
    let files = vec![AdaFile::from(PathBuf::from(spec_path)),
                     AdaFile::from(PathBuf::from(body_path))];
    let files = adafmt::organize_imports(&files);
    adafmt::write_back_ada_files(files);

    let good_spec = read_file("tests/resources/test.ads_good");
    let good_body = read_file("tests/resources/test.adb_good");
    println!("{:?}", read_file(body_path));
    println!("And the good one", );
    println!("{:?}", good_body);
    assert!(read_file(spec_path).eq(&good_spec));
    assert!(read_file(body_path).eq(&good_body));

    // Clean up
    write_file(spec_path, read_file("tests/resources/test.ads_orig"));
    write_file(body_path, read_file("tests/resources/test.adb_orig"));

}
