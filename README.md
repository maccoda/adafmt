# Adafmt
The beginnings of what hopes to be a useful formatter for Ada, not written in Ada but in Rust.

Goal is to be able to run this tool from the command line on an Ada project and be able to keep track of changes made during the format to show to the user.

## Capabilities
- [x] Organize imports
- [ ] General formatting
  * Functions/Procedures
  * Parameters
  * Conditional blocks
- [ ] Guideline checks
  * Variable naming
  * Function/Procedure naming
  * Attributes

## Usage
Once the binary is built the tool can be executed from the command line with the root directory of the project provided. Further it will take option argument to specify different functionality.

`adafmt /path/to/project`

For more information on available options run

`adafmt --help`
